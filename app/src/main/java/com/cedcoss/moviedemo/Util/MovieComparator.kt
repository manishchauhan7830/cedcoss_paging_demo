package com.cedcoss.moviedemo.Util

import androidx.recyclerview.widget.DiffUtil
import com.cedcoss.moviedemo.model.Movie


class MovieComparator : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }
}