package com.cedcoss.moviedemo.ViewModel

import androidx.paging.rxjava3.flowable
import androidx.lifecycle.viewModelScope
import androidx.paging.rxjava3.cachedIn
import androidx.lifecycle.ViewModel
import androidx.paging.PagingData
import com.cedcoss.moviedemo.Paging.MoviePagingSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.cedcoss.moviedemo.model.Movie
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

class MainActivityViewModel : ViewModel() {
    var pagingDataFlow: Flowable<PagingData<Movie>>? = null


    init {
            val moviePagingSource = MoviePagingSource()
            val pager: Pager<Int, Movie> = Pager<Int, Movie>(
                PagingConfig(
                    20,
                    20,
                    false,
                    20,
                    20 * 499
                ),
                pagingSourceFactory = {moviePagingSource})

            // inti Flowable
            pagingDataFlow = pager.flowable
            val coroutineScope: CoroutineScope = this.viewModelScope
            pagingDataFlow!!.cachedIn<Movie>(coroutineScope)
    }



}
