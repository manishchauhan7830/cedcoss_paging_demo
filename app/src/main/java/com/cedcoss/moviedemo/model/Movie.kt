package com.cedcoss.moviedemo.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


class Movie {
    @SerializedName("imdbID")
    @Expose
    var id: String? = null

    @SerializedName("Poster")
    @Expose
    var posterPath: String? = null

    @SerializedName("Title")
    @Expose
    var voteAverage: String? = null
    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        return if (other === this) true else false
    }
}