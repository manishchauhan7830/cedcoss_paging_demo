package com.cedcoss.moviedemo.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


class MovieResponse {
    @SerializedName("Search")
    @Expose
    var results: List<Movie>? = null

    @SerializedName("totalResults")
    @Expose
    var totalResults: String? = null
}