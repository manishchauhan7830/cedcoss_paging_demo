package com.cedcoss.moviedemo.Adapter

import androidx.paging.LoadStateAdapter
import com.cedcoss.moviedemo.Adapter.MoviesLoadStateAdapter.LoadStateViewHolder
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import com.cedcoss.moviedemo.R
import android.widget.ProgressBar
import android.widget.TextView
import com.cedcoss.moviedemo.databinding.LoadStateItemBinding

class MoviesLoadStateAdapter(private val mRetryCallback: View.OnClickListener) :
    LoadStateAdapter<LoadStateViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadStateViewHolder {
        return LoadStateViewHolder(parent, mRetryCallback)
    }

    override fun onBindViewHolder(
        holder: LoadStateViewHolder,
        loadState: LoadState
    ) {
        holder.bind(loadState)
    }

    class LoadStateViewHolder internal constructor(
        parent: ViewGroup,
        retryCallback: View.OnClickListener
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.load_state_item, parent, false)
    ) {
        private val mProgressBar: ProgressBar
        private val mErrorMsg: TextView
        private val mRetry: Button
        fun bind(loadState: LoadState?) {
            if (loadState is LoadState.Error) {
                val loadStateError = loadState as LoadState.Error
                mErrorMsg.setText(loadStateError.toString())
            }
            mProgressBar.visibility =
                if (loadState is LoadState.Loading) View.VISIBLE else View.GONE
            mRetry.visibility =
                if (loadState is LoadState.Error) View.VISIBLE else View.GONE
            mErrorMsg.visibility =
                if (loadState is LoadState.Error) View.VISIBLE else View.GONE
        }

        init {
            val binding = LoadStateItemBinding.bind(itemView)
            mProgressBar = binding.progressBar
            mErrorMsg = binding.errorMsg
            mRetry = binding.retryButton
            mRetry.setOnClickListener(retryCallback)
        }
    }
}