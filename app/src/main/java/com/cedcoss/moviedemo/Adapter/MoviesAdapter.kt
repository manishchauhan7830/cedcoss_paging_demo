package com.cedcoss.moviedemo.Adapter

import androidx.paging.PagingDataAdapter
import com.cedcoss.moviedemo.Adapter.MoviesAdapter.MovieViewHolder
import android.view.ViewGroup
import android.view.LayoutInflater
import com.squareup.picasso.Picasso
import androidx.recyclerview.widget.RecyclerView
import com.cedcoss.moviedemo.model.Movie
import com.cedcoss.moviedemo.Util.MovieComparator
import com.cedcoss.moviedemo.databinding.MovieItemBinding


class MoviesAdapter(diffCallback: MovieComparator) :
    PagingDataAdapter<Movie, MovieViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            MovieItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val currentMovie = getItem(position)
        if (currentMovie != null) {
            Picasso.get().load(currentMovie.posterPath)
                .fit().into(holder.movieItemBinding.imageViewMovie)
            holder.movieItemBinding.textViewRating.text = currentMovie.voteAverage.toString()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount) MOVIE_ITEM else LOADING_ITEM
    }

    inner class MovieViewHolder(var movieItemBinding: MovieItemBinding) : RecyclerView.ViewHolder(
        movieItemBinding.root
    )

    companion object {
        const val LOADING_ITEM = 0
        const val MOVIE_ITEM = 1
    }
}