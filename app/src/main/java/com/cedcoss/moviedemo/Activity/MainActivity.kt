package com.cedcoss.moviedemo.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.cedcoss.moviedemo.Adapter.MoviesAdapter
import com.cedcoss.moviedemo.Util.MovieComparator
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.cedcoss.moviedemo.Util.GridSpace
import com.cedcoss.moviedemo.Adapter.MoviesLoadStateAdapter
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cedcoss.moviedemo.R
import com.cedcoss.moviedemo.model.Movie
import com.cedcoss.moviedemo.ViewModel.MainActivityViewModel
import com.cedcoss.moviedemo.databinding.ActivityMainBinding
import io.reactivex.rxjava3.functions.Consumer


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(
            layoutInflater
        )
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)


        val moviesAdapter = MoviesAdapter(MovieComparator())

        val mainActivityViewModel = ViewModelProvider(this).get(
            MainActivityViewModel::class.java
        )

       mainActivityViewModel.pagingDataFlow!!.subscribe(Consumer { moviePagingData: PagingData<Movie> ->
                moviesAdapter.submitData(lifecycle, moviePagingData)

            })



        val gridLayoutManager = GridLayoutManager(this, 2)

        binding.recyclerViewMovies.layoutManager = gridLayoutManager

        binding.recyclerViewMovies.addItemDecoration(GridSpace(2, 20, true))

        binding.recyclerViewMovies.adapter =
            moviesAdapter.withLoadStateFooter(

                MoviesLoadStateAdapter { v: View? -> moviesAdapter.retry() })


        gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {

                return if (moviesAdapter.getItemViewType(position) == MoviesAdapter.LOADING_ITEM) 1 else 2
            }
        }
    }
}