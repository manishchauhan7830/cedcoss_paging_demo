package com.cedcoss.moviedemo.Paging

import androidx.paging.rxjava3.RxPagingSource
import com.cedcoss.moviedemo.API.APIClient
import com.cedcoss.moviedemo.model.Movie
import com.cedcoss.moviedemo.model.MovieResponse
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.Exception

class MoviePagingSource : RxPagingSource<Int, Movie>() {
    override fun loadSingle(loadParams: LoadParams<Int>): Single<LoadResult<Int, Movie>> {
        return try {
            val page = if (loadParams.key != null) loadParams.key!! else 1
            APIClient.aPIInterface!!
                .getMoviesByPage(page)
                .subscribeOn(Schedulers.io())
                .map(MovieResponse::results)
                .map { movies: List<Movie>? ->
                    toLoadResult(
                        movies!!,
                        page
                    )
                }
                .onErrorReturn { LoadResult.Error(it) }

        }
        catch (e:Exception)
        {
            return Single.just(LoadResult.Error(e))
        }
    }

    // Method to map Movies to LoadResult object
    private fun toLoadResult(movies: List<Movie>, page: Int): LoadResult<Int, Movie> {
        return LoadResult.Page(movies, if (page == 1) null else page - 1, page + 1)
    }
}