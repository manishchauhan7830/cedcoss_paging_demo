package com.cedcoss.moviedemo.API

import com.cedcoss.moviedemo.API.APIInterface
import com.cedcoss.moviedemo.API.APIClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.Interceptor
import okhttp3.HttpUrl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory


object APIClient {

    var apiInterface: APIInterface? = null

    val aPIInterface: APIInterface?
        get() {

            if (apiInterface == null) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                client.addInterceptor(logging)

                client.addInterceptor { chain: Interceptor.Chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url()
                    val url = originalHttpUrl.newBuilder()
                        .build()
                    val requestBuilder = original.newBuilder()
                        .url(url)
                    val request = requestBuilder.build()
                    chain.proceed(request)
                }
                val retrofit = Retrofit.Builder() // set base url
                    .baseUrl("http://www.omdbapi.com/")
                    .client(client.build()) // Add Gson converter
                    .addConverterFactory(GsonConverterFactory.create()) // Add RxJava spport for Retrofit
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build()

                // Init APIInterface
                apiInterface = retrofit.create(APIInterface::class.java)
            }
            return apiInterface
        }
}