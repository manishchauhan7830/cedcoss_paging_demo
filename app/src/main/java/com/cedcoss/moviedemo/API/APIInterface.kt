package com.cedcoss.moviedemo.API

import retrofit2.http.GET
import com.cedcoss.moviedemo.model.MovieResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Query


interface APIInterface {
    @GET("?apikey=1d094e25&s=fast&type=movie&")
    fun getMoviesByPage(@Query("page") page: Int): Single<MovieResponse>
}